class Bank:                                                     #CLASS     
    def __init__(self,nama,jenis_akun, jmlh_bencoin=0):     #Constructor Class        
        self.nama=nama                  #nama akun             
        self.jenis_akun=jenis_akun      #jenis akun
        self.jmlh_bencoin=jmlh_bencoin  #jumlah saldo
        self.riwayat=[]                 #tampungan riwayat transaksi
        akun[self.nama]=jenis_akun      #membuat dict nama akun dengan rjenis akun nya
        if self.jenis_akun=='pelajar':  #Ketentuan untuk akun Pelajar
            self.batas_tabungan=150
            self.batas_transaksi=25
            self.biaya= 0
        elif self.jenis_akun=='reguler':#Ketentuan untuk akun Reguler
            self.batas_tabungan=500
            self.batas_transaksi=100
            self.biaya= 5
        elif self.jenis_akun=='bisnis': #Ketentuan untuk akun Bisnis
            self.batas_tabungan=2000
            self.batas_transaksi=500
            self.biaya= 15
        elif self.jenis_akun=='elite':  #Ketentuan untuk akun Elite
            self.batas_tabungan=100000
            self.batas_transaksi=10000
            self.biaya= 50

    def tambah(self,rate):          #fungsi TAMBAH
        jenis_uang[self]=rate       #membuat dict nama uang dengan rate nya
        
    def setor(self,nama,uang,mata_uang):        #fungsi SETOR
            if int(uang)<0:         #input harus positif
                print('Setor ditolak')
            elif self.jmlh_bencoin==self.batas_tabungan:  #disaat saldo sudah melewati batas tabungan
                print('Saldo akun {} telah melebihi batas'.format(self.nama.capitalize()))
            elif self.jmlh_bencoin + (int(uang)/int(jenis_uang[mata_uang])) > self.batas_tabungan:  #disaat menyetor melewati batas tabungan
                print('Akun {} telah bertambah {} BenCoin'.format(self.nama.capitalize(),float(self.batas_tabungan-int(self.jmlh_bencoin))))
                self.riwayat.append('SETOR {} {} {}'.format(mata_uang.upper(),uang,float(self.batas_tabungan-int(self.jmlh_bencoin))))  #menambahkan info transaksi ke riwayat transaksi
                self.jmlh_bencoin = self.batas_tabungan
            else:                               #menyetor normal
                self.jmlh_bencoin += (int(uang)/int(jenis_uang[mata_uang]))
                print('Akun {} telah bertambah {} BenCoin'.format(self.nama.capitalize(),(int(uang)/int(jenis_uang[mata_uang]))))
                self.riwayat.append('SETOR {} {} {}'.format(mata_uang.upper(),uang,float(int(uang)/int(jenis_uang[mata_uang]))))    #menambahkan info transaksi ke riwayat transaksi

    def tarik(self,nama, uangbtc, mata_uang):   #fungsi TARIK
            if int(uangbtc)<0:      #input harus positif
                print('Tidak bisa tarik')
            elif self.jmlh_bencoin <self.biaya:   #Saldo kurang dr biaya transaksi
                print('Saldo tidak cukup')
            elif int(uangbtc) > self.batas_transaksi: #tarik lebih dari batas
                uangbtc= int(self.batas_transaksi)
                self.jmlh_bencoin-=(int(uangbtc)+int(self.biaya))
                print('Penarikan {} dari akun {} berhasil '.format((int(uangbtc)*int(jenis_uang[mata_uang])),self.nama.capitalize()))
                self.riwayat.append('TARIK {} {} {}'.format(mata_uang.upper(),(int(uangbtc)*int(jenis_uang[mata_uang])),uangbtc))
            elif int(uangbtc) >= self.jmlh_bencoin:  #Saldo kurang dr yang ingin ditarik, sehingga jumlah saldo menjadi 0 serta yang ditarik cuman yang bisa saja
                print('Penarikan {} dari akun {} berhasil '.format((int(self.jmlh_bencoin-self.biaya)*int(jenis_uang[mata_uang])),self.nama.capitalize()))
                self.riwayat.append('TARIK {} {} {}'.format(mata_uang.upper(),(int(self.jmlh_bencoin-int(self.biaya))*int(jenis_uang[mata_uang])),int(self.jmlh_bencoin-int(self.biaya))))    #menambahkan info transaksi ke riwayat transaksi
                self.jmlh_bencoin =0
            else:                               #tarik saldo normal
                self.jmlh_bencoin -= (int(uangbtc)+self.biaya)
                print('Penarikan {} dari akun {} berhasil '.format((int(uangbtc)*int(jenis_uang[mata_uang])),self.nama.capitalize()))
                self.riwayat.append('TARIK {} {} {}'.format(mata_uang.upper(),(int(uangbtc)*int(jenis_uang[mata_uang])),uangbtc))       #menambahkan info transaksi ke riwayat transaksi

    def transfer(self, penerima,bencoin):       #fungsi TRANSFER
            if int(bencoin)<0:      #input harus positif
                print('Transaksi gagal')
            elif int(self.jmlh_bencoin) <= self.biaya:     #saldo kurang dr biaya transaksi
                print('Akun {} tidak cukup untuk melakukan transfer'.format(self.nama))
            elif int(bencoin) <= self.biaya:        #transferan kurang dari biaya transaksi
                print('Transaksi gagal')
            elif (int(bencoin)+int(self.biaya)) >= int(self.jmlh_bencoin):         #jumlah transferan+biaya melebihi saldo
                penerima.jmlh_bencoin+= (int(self.jmlh_bencoin)-int(self.biaya))
                self.riwayat.append('TRANSFER {} {}'.format(penerima.nama.capitalize(),int(self.jmlh_bencoin)-int(self.biaya)))        #menambahkan info transaksi ke riwayat transaksi
                penerima.riwayat.append('MENERIMA TRANSFER DARI {} {}'.format(self.nama.capitalize(),int(int(self.jmlh_bencoin)-int(self.biaya))))  #menambahkan info transaksi ke riwayat transaksi
                print('{} berhasil mentransfer {} BenCoin kepada {}'.format(self.nama.capitalize(),int(self.jmlh_bencoin)-int(self.biaya),penerima.nama.capitalize()))
                self.jmlh_bencoin= 0
            elif int(penerima.jmlh_bencoin)+ int(bencoin)>= int(penerima.batas_tabungan):     #saldo penerima sudah melewati batas tabungannya
                bencoin = int(penerima.batas_tabungan)-int(penerima.jmlh_bencoin)
                self.jmlh_bencoin-= int(bencoin) + int(self.biaya)
                penerima.jmlh_bencoin= penerima.batas_tabungan
                self.riwayat.append('TRANSFER {} {}'.format(penerima.nama.capitalize(),int(bencoin)))        #menambahkan info transaksi ke riwayat transaksi
                penerima.riwayat.append('MENERIMA TRANSFER DARI {} {}'.format(self.nama.capitalize(),int(bencoin)))  #menambahkan info transaksi ke riwayat transaksi
                print('{} berhasil mentransfer {} BenCoin kepada {}'.format(self.nama.capitalize(),int(bencoin),penerima.nama.capitalize()))   
            elif int(bencoin) >self.batas_transaksi:            #jumlah transferan melewati batas transaksi
                bencoin=self.batas_transaksi
                self.jmlh_bencoin-=(int(bencoin)+int(self.biaya))
                penerima.jmlh_bencoin+=(int(bencoin))
                self.riwayat.append('TRANSFER {} {}'.format(penerima.nama.capitalize(),int(bencoin)))        #menambahkan info transaksi ke riwayat transaksi
                penerima.riwayat.append('MENERIMA TRANSFER DARI {} {}'.format(self.nama.capitalize(),int(bencoin)))  #menambahkan info transaksi ke riwayat transaksi
                print('{} berhasil mentransfer {} BenCoin kepada {}'.format(self.nama.capitalize(),int(bencoin),penerima.nama.capitalize()))
            else:                                               #transfer normal
                self.jmlh_bencoin-=(int(bencoin)+int(self.biaya))
                penerima.jmlh_bencoin+=(int(bencoin)-int(self.biaya))
                self.riwayat.append('TRANSFER {} {}'.format(penerima.nama.capitalize(),int(bencoin)-int(self.biaya)))        #menambahkan info transaksi ke riwayat transaksi
                penerima.riwayat.append('MENERIMA TRANSFER DARI {} {}'.format(self.nama.capitalize(),int(bencoin)-int(self.biaya)))  #menambahkan info transaksi ke riwayat transaksi
                print('{} berhasil mentransfer {} BenCoin kepada {}'.format(self.nama.capitalize(),int(bencoin)-int(self.biaya),penerima.nama.capitalize()))
            
        
    def info(self,jmlh_bencoin):    #fungsi INFO
        print('Nama           : {}'.format(self.nama.capitalize()))
        print('Jenis Akun     : {}'.format(self.jenis_akun.capitalize()))
        print('Jumlah BenCoin : {}'.format(self.jmlh_bencoin))
        print('Transaksi:')
        for riwayat in self.riwayat:    #iterasi tampungan riwayat transaksi
            print(riwayat)
            
akun={}         #dict akun dengan object jenis akun    
jenis_uang={}   #dict nama uang dengan rate

print('------SELAMAT DATANG DI BANK ALI------')
while (True):
    try:        #mencegah ada kesalahan input lainya
        inp = input('').lower().split()     #mengecilkan semua huruf inputan dan di split
        if (inp[0] == 'daftar'):
            if inp[2]=='pelajar' or inp[2]=='reguler' or inp[2]=='bisnis' or inp[2]=='elite':   #Pilihan Paket yang tepat
                if inp[1] in akun:      #memastikan agar akun yang sama tidak terdaftar 2x
                    print('Akun {} sudah terdaftar'.format(inp[1].capitalize()))
                else:
                    Akun= Bank(inp[1],inp[2])   #menjadikan object di class
                    akun[inp[1]]=Akun
                    print('Akun atas nama {} telah terdaftar dengan paket {}'.format((inp[1].capitalize()), inp[2].capitalize()))
            else:       #kalau salah paket
                print('Pilihan paket tidak tepat!')     
        elif inp[0] == 'tambah':    
            if inp[1] not in jenis_uang:    #memastikan kalau mata uang baru terdaftar 1x
                Bank.tambah(inp[1],int(inp[2]))
                print( 'Mata uang {} telah ditambahkan dengan rate {} per BenCoin'.format(inp[1].upper(),inp[2]))
            else:       #kalau mata uang sudah ditambahkan
                print('Mata uang {} sudah ditambahkan!'.format(inp[1].upper()))
        elif inp[0] == 'ubah':
            if inp[1] in jenis_uang:        #memastikan kalau mata uang telah terdafar
                Bank.tambah(inp[1],int(inp[2]))
                print( 'Rate mata uang {} berubah menjadi {} per BenCoin.'.format(inp[1].upper(),inp[2]))
            else:
                print('Mata uang {} belum terdaftar'.format(inp[1].upper()))          
        elif inp[0] == 'setor':
            if inp[1] in akun:      #memastikan akun sudah terdaftar
                if inp[3] in jenis_uang:    #memastikan mata uang telah terdaftar
                    akun[inp[1]].setor([inp[1]],inp[2],inp[3])
                else:   #kalau mata uang belom terdaftar
                    print('Mata uang {} belum terdaftar!'.format(inp[3].upper()))
            else:   #kalau akun belom terdaftar
                print('Akun atas nama {} belum terdaftar!'.format(inp[1].capitalize()))      
        elif inp[0] == 'tarik':
            if inp[1] in akun:      #memastikan akun sudah terdaftar
                if inp[3] in jenis_uang:    #memastikan uang telah terdaftar
                    akun[inp[1]].tarik([inp[1]],inp[2],inp[3])
                else:   #kalau mata uang belum terdaftar
                    print('Mata uang {} belum terdaftar!'.format(inp[3].upper()))
            else:   #kalau akun belum terdaftar
                print('Akun atas nama {} belum terdaftar!'.format(inp[1].capitalize()))          
        elif inp[0] == 'transfer':
            if inp[1] in akun and inp[2] in akun:   #memastikan kedua akun telah terdaftar
                    akun[inp[1]].transfer(akun[inp[2]],inp[3])
            else:   #Kalau ada akun yang belom terdaftar
                print('Kesalahan nama akun!')
        elif inp[0] == 'info':
            if inp[1] in akun:  #memastikan akun telah terdaftar
                akun[inp[1]].info(inp[1])
            else:   #kalau akun belom terdaftar
                print('Akun {} belum terdaftar'.format(inp[1]))
        elif inp[0] == 'exit':
            print('-------------TERIMA KASIH-------------')
            break
        else:       #Kalau salah perintah
            print('Masukan perintah yang tepat!')
    except:     #kalau ada kesalahan lain
        print('Sepertinya ada kesalahan input!')
        
    


            
