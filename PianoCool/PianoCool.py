from tkinter import *           #import semua isi dari tkinter
import pygame                   #import pygame
import time
import os
os.chdir('C:\\Users\\asus\\Music\\ALI\\')
pygame.mixer.init()
print('.---------------------Created by Ali Yusuf----------------------.\n|GUIDE:\t\t\t\t\t\t\t\t|\n|a = C\ts = D\td = E\tf = F\tg = G\th = A\tj = B\t\t|\n|k = C\'\tq = Db\tw = Eb\te = Gb\tr = Ab\tt = Bb\tenter = +oktaf\t|\n`---------------------------------------------------------------`')
class Piano:
    def __init__(self, master):
        self.master = master
        master.title("PIANO TILE by Ali Yusuf")
        master.geometry("490x360")
        master.config(bg='silver')
        #bagian entry
        self.txt=StringVar()    #isi entry
        self.isi=Entry(master,bd=10,width=25,text=self.txt,selectbackground='green')    #membuat entry
        self.button_isi=Button(master, text='PLAY!!',command=self.tes,bg='blue',fg='white')     #button play
        self.isi.place(x=150,y=10)
        self.button_isi.place(x=330,y=15)
        #bagian oktaf
        self.nilai= IntVar()    #nilai oktaf
        self.nilai.set(5)       #set menjadi oktaf 5
        oktaf=[1,2,3,4,5,6,7,8]     #list oktaf
        for i in range(7):
            ok= Radiobutton(master, text=i+1,variable=self.nilai,value=oktaf[i], bg='silver')   #membuat radiobutton
            ok.place(x=57*(i+1),y=50)
        #bagian tuts1
        self.tuts1=['C','D','E','F','G','A','B','C\'']  #list tuts 
        warna=['#555c9d','#8c8a7d','#6c5b7b','#c06c84','#f67280','#f8b195','#eabeda','#eaeaff'] #list warna
        for i in range(8):
            def efek2(x=self.tuts1[i]): #fungsi untuk memainkan musik
                if x=='C\'':
                    p=pygame.mixer.Sound('C'+str(self.nilai.get()+1)+'.wav')
                    p.play()
                    p.fadeout(1000)     #agar dsingkat
                else:
                    p=pygame.mixer.Sound(str(x)+str(self.nilai.get())+'.wav')
                    p.play()
                    p.fadeout(1000)
            ts1=Button(master,text=self.tuts1[i],command=efek2,padx=22,pady=125,bg=warna[i], relief=FLAT,bd=3,activebackground=warna[i])    #button tuts1
            ts1.place(x=62*(i), y=85)
        #bagian tuts2
        self.tuts2=['C#','D#','F#','G#','A#']       #list tuts
        self.tuts2s=['Db','Eb','Gb','Ab','Bb']      #list tuts yg sesuai file musik
        for i in range(5):
            def efek3(x=self.tuts2s[i]):        #fungsi buat main musik
                p=pygame.mixer.Sound(str(x)+str(self.nilai.get())+'.wav')
                p.play()
                p.fadeout(1000)
            if i>=2:        #disaat tuts lebih dr 2 dikasih longkapan terlebih dahulu
                ts2=Button(master,text=self.tuts2[i],command=efek3,padx=8,pady=75,bg='black',fg='white',relief=FLAT,bd=3,activebackground='black',activeforeground='white') #button tuts
                ts2.place(x=102+(i*62), y=85)
            else:
                ts2=Button(master,text=self.tuts2[i],command=efek3,padx=8,pady=75,bg='black',fg='white',relief=FLAT,bd=3,activebackground='black',activeforeground='white') #button tuts
                ts2.place(x=40+(i*62), y=85)  
        #bagian pencet keyboard
        master.bind('<a>', lambda event:self.bunyi('C'+str(self.nilai.get())))
        master.bind('<s>', lambda event:self.bunyi('D'+str(self.nilai.get())))
        master.bind('<d>', lambda event:self.bunyi('E'+str(self.nilai.get())))
        master.bind('<f>', lambda event:self.bunyi('F'+str(self.nilai.get())))
        master.bind('<g>', lambda event:self.bunyi('G'+str(self.nilai.get())))
        master.bind('<h>', lambda event:self.bunyi('A'+str(self.nilai.get())))
        master.bind('<j>', lambda event:self.bunyi('B'+str(self.nilai.get())))
        master.bind('<k>', lambda event:self.bunyi('C'+str(self.nilai.get()+1)))
        master.bind('<q>', lambda event:self.bunyi('Db'+str(self.nilai.get())))
        master.bind('<w>', lambda event:self.bunyi('Eb'+str(self.nilai.get())))
        master.bind('<e>', lambda event:self.bunyi('Gb'+str(self.nilai.get())))
        master.bind('<r>', lambda event:self.bunyi('Ab'+str(self.nilai.get())))
        master.bind('<t>', lambda event:self.bunyi('Bb'+str(self.nilai.get())))
        master.bind('<Return>', self.oktaf)
    def oktaf(self,event=None):     #def untuk ganti oktaf tinggal pencet enter
        if self.nilai.get()==7: #kalau oktaf udh ke 7, di reset jd oktaf 1 
            self.nilai.set(1)
        else:
            self.nilai.set(self.nilai.get()+1)  #nambahin oktaf 1 tingkatan
    def bunyi(self,tuts,event=None):        #def untuk memainkan lewat keyboard
        p=pygame.mixer.Sound(str(tuts)+'.wav')
        p.play()
        p.fadeout(1000)
    def tes(self):          #def pengganti tuts yg diinput dengan sesuai nama file yang ada
            isian=self.txt.get()
            isian2={'C#':'Db','D#':'Eb','F#':'Gb','G#':'Ab','A#':'Bd'}
            for x,y in isian2.items():
                isian=isian.replace(x,y)    #isinya di replace dr dict
            isian=isian.split('-')
            for i in isian:
                    try:
                        p=pygame.mixer.Sound(str(i)+'.wav')     #play music sesuai input
                        p.play()
                        p.fadeout(1000)
                        time.sleep(0.5) #memberi jeda 0.5 detik
                    except:
                        pass     
root = Tk()
piano = Piano(root)
root.mainloop()
