import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.Timer;

public class ImageButtonListener implements ActionListener {
    private Permainan kartu;
    public int winner = 0;

    public ImageButtonListener(Permainan kartu) {
        this.kartu = kartu;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int numClicks = kartu.getNumClicks();
        ArrayList<JToggleButton> buttons = kartu.getButtons();
        ImageIcon[] fileImg = kartu.getFileImg();
        ArrayList<ImageIcon> foto = kartu.getFoto();
        Timer myTimer = kartu.getMyTimer();
        Timer myTimer2 = kartu.getMyTimer2();
        JLabel label = kartu.getLabel();
        int winner = kartu.getWinner();

        /**
         * kalo timer jalan, gabisa ngelakuin apa2
         */
        if (myTimer.isRunning()) {
            return;
        }
        if (myTimer2.isRunning()) {
            return;
        }
        /**
         * penambahan counter klik
         */
        kartu.setNumClicks(numClicks += 1);


        // kalo ganjil        }
        if (numClicks % 2 == 1) {
            for (int i = 0; i < fileImg.length; i++) {
                if (e.getSource() == buttons.get(i)) { // find
                    buttons.get(i).setIcon(foto.get(i)); // seticon yg telah dishuffle
                    kartu.setGanjil(i); // set index ganjil
                }
            }
        }
        // kalo genap
        else if (numClicks % 2 == 0) {
            for (int i = 0; i < fileImg.length; i++) {
                if (e.getSource() == buttons.get(i)) { // find
                    buttons.get(i).setIcon(foto.get(i)); // seticon yg telah dishuflle
                    kartu.setGenap(i); // set index genap
                }
            }
            if (kartu.getGanjil() == kartu.getGenap()) { // kalo index ganjil&genap sama
                kartu.setNumClicks(numClicks -= 1);
            }
            else if (foto.get(kartu.getGanjil()).getDescription()
                    .equals((foto.get(kartu.getGenap()).getDescription()))) { // kalo foto dengan id
                // yang sama
                myTimer2.start(); // delay
                label.setText("Tries: " + String.valueOf(numClicks / 2));
                kartu.setWinner(winner += 1); // count point+1
                if(kartu.getWinner()==18) {     //kalo udh kena semua
                    JOptionPane.showMessageDialog(null,"YOU WIN");
                }

            } else if (fileImg[kartu.getGanjil()] != fileImg[kartu.getGenap()]) { // kl beda
                myTimer.start(); // delay
                label.setText("Tries: " + String.valueOf(numClicks / 2));
            }

        }

    }

}
