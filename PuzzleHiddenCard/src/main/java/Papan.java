import java.awt.Color;
import java.awt.Dimension;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Papan extends JFrame {
    /**
     *
     */
    private static final long serialVersionUID = 3423150602582504849L;

    public Papan() throws IOException {

        Permainan game= new Permainan(this);        //inisiasi
        setTitle("Memory Game");
        setBackground(Color.CYAN);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(new Dimension(400, 300));
        setResizable(false);    //biar gabisa di resize
        add(game);
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    new Papan();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
