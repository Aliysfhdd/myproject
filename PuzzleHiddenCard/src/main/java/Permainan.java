import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.Timer;

class Permainan extends JPanel {

    /**
     *
     */
    private static final long serialVersionUID = 1980126109272694120L;
    private int numClicks = 0;
    private int ganjil = 0;
    private int genap = 0;
    private Timer myTimer;
    private Timer myTimer2;
    private ImageIcon[] fileImg = { image("1.gif"), image("2.png"), image("3.jpg"), image("4.jpg"),
            image("5.jpg"), image("6.jpg"), image("7.jpg"), image("8.png"), image("9.jpg"),
            image("10.jpg"), image("11.jpg"), image("12.jpg"), image("13.jpg"), image("14.jpg"),
            image("15.jpg"), image("16.jpg"), image("17.jpg"), image("18.jpg"), image("1.gif"),
            image("2.png"), image("3.jpg"), image("4.jpg"), image("5.jpg"), image("6.jpg"),
            image("7.jpg"), image("8.png"), image("9.jpg"), image("10.jpg"), image("11.jpg"),
            image("12.jpg"), image("13.jpg"), image("14.jpg"), image("15.jpg"), image("16.jpg"),
            image("17.jpg"), image("18.jpg") };
    private ImageIcon imgawal = image("main.jpg");
    private JLabel label = new JLabel("Tries: " + String.valueOf(numClicks));
    private ArrayList<ImageIcon> foto = new ArrayList<ImageIcon>(Arrays.asList(fileImg));
    private ArrayList<JToggleButton> buttons = new ArrayList<JToggleButton>();
    private int winner = 0;

    /**
     * This is for getter
     *
     * @return masing2 tipe
     */

    public int getWinner() {
        return winner;
    }

    public void setWinner(int i) {
        this.winner = i;
    }

    public int getNumClicks() {
        return numClicks;
    }

    public void setNumClicks(int i) {
        this.numClicks = i;
    }

    public int getGanjil() {
        return ganjil;
    }

    public void setGanjil(int i) {
        this.ganjil = i;
    }

    public int getGenap() {
        return genap;
    }

    public void setGenap(int i) {
        this.genap = i;
    }

    public Timer getMyTimer() {
        return myTimer;
    }

    public Timer getMyTimer2() {
        return myTimer2;
    }

    public ImageIcon[] getFileImg() {
        return fileImg;
    }

    public ImageIcon getImgawal() {
        return imgawal;
    }

    public JLabel getLabel() {
        return label;
    }

    public ArrayList<ImageIcon> getFoto() {
        return foto;
    }

    public ArrayList<JToggleButton> getButtons() {
        return buttons;
    }

    /**
     * Concstructor
     *
     * @throws IOException
     */
    public Permainan(Papan frame) throws IOException {
        this.setLayout(new BorderLayout()); // setlayout
        JPanel card = new JPanel(); // add new panel
        card.setBackground(Color.LIGHT_GRAY);
        int total = fileImg.length;
        card.setLayout(new GridLayout(6, 6));
        card.setPreferredSize(new Dimension(500, 500));

        /**
         * looping 36x, dan di bikin button tiap loop+ add&set button tsb ke arraylist
         * dengan gambar tertutup
         */

        for (int i = 0; i < total; i++) {
            JToggleButton button = new JToggleButton(imgawal);
            buttons.add(button);
            button.addActionListener(new ImageButtonListener(this));
            card.add(button, BorderLayout.NORTH);
            setVisible(true);
        }

        /**
         * Memberi id(deskripsi) tiap list foto yang akan ditambah ke button kalo udh
         * lebih dr 18, ke ulang dr 1.
         */

        for (int i = 0; i < foto.size(); i++) {
            if (i > 17) {
                foto.get(i).setDescription(String.valueOf(i - 18));
            } else {
                foto.get(i).setDescription(String.valueOf(i));
            }
        }

        Collections.shuffle(foto); // acak foto
        this.add(card, BorderLayout.NORTH); // add panel banyak kartu
        JPanel buttonlain = new JPanel();
        JToggleButton reset = new JToggleButton("RESET"); // button reset
        JToggleButton exit = new JToggleButton("EXIT");
        exit.setPreferredSize(new Dimension(100, 50));
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.dispose();
            }
        });

        reset.setPreferredSize(new Dimension(100, 50));
        ;
        reset.addActionListener(new ResetListener(this));
        buttonlain.add(reset);
        buttonlain.add(exit);
        add(buttonlain, BorderLayout.CENTER);
        setVisible(true);
        label.setForeground(Color.BLUE);
        add(label, BorderLayout.SOUTH);
        setVisible(true);
        myTimer = new Timer(700, new TimerListener(this)); // timer utk !=
        myTimer2 = new Timer(400, new TimerListener2(this)); // timer utk ==

    }

    public ImageIcon image(String img) throws IOException {
        try {
            return new ImageIcon(ImageIO.read(new File("assignment-4\\img\\" + img))
                    .getScaledInstance(90, 90, Image.SCALE_SMOOTH));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error in find Image\nPlease place image in img folder");
            System.exit(0);
            return null;
        }
    }

}