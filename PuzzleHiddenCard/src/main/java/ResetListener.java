import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JToggleButton;

public class ResetListener implements ActionListener {
    Permainan kartu;

    public ResetListener(Permainan kartu) {
        this.kartu = kartu;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ArrayList<JToggleButton> buttons = kartu.getButtons();
        JLabel label= kartu.getLabel();
        ImageIcon imgawal= kartu.getImgawal();

        for (JToggleButton i : buttons) {   //loop tiap button dan set ulang
            i.setIcon(imgawal);
            i.setVisible(true);
        }
        Collections.shuffle(buttons);   //acak lg
        kartu.setNumClicks(0);  //set ke 0
        kartu.setWinner(0);     //set ke 0
        label.setText("Tries: "+String.valueOf(0));

    }

}


