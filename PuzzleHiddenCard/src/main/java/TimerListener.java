import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JToggleButton;
import javax.swing.Timer;

public class TimerListener implements ActionListener {
    Permainan kartu;

    public TimerListener(Permainan kartu) {
        this.kartu = kartu;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ArrayList<JToggleButton> buttons = kartu.getButtons();
        int ganjil= kartu.getGanjil();
        int genap= kartu.getGenap();
        Timer myTimer = kartu.getMyTimer();
        ImageIcon imgawal= kartu.getImgawal();

        buttons.get(ganjil).setIcon(imgawal);   //balik kartu lg
        buttons.get(genap).setIcon(imgawal);
        myTimer.stop();     //stop timer
    }
}