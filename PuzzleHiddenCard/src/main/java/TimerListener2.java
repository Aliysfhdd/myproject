import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JToggleButton;
import javax.swing.Timer;

public class TimerListener2 implements ActionListener {
    Permainan kartu;


    public TimerListener2(Permainan kartu) {
        this.kartu = kartu;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ArrayList<JToggleButton> buttons = kartu.getButtons();
        int ganjil= kartu.getGanjil();
        Timer myTimer2 = kartu.getMyTimer2();
        int genap= kartu.getGenap();

        buttons.get(ganjil).setVisible(false);  //ilangin
        buttons.get(genap).setVisible(false);   //ilangin
        myTimer2.stop();    //stop timer
    }
}
