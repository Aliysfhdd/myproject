import turtle            #import file turtle
import random            #import file random
ts = turtle.Screen()                                                
t= turtle.Turtle()
ts.colormode(255)       #menentukan jangkauan value warna
t.speed(0)              #memnggunakan kecepatan tertinggi
ts.title('Colorful Chessboard and Flower')      #judul program
petal = ts.numinput("Colorful Chessboard and Flower", "Enter the number of petals of the flowers:")  #input petal
pixel = ts.numinput("Colorful Chessboard and Flower", "Enter the square size (pixels):")             #input pixel persegi
row = ts.numinput("Colorful Chessboard and Flower", "Enter the number of rows:")                     #input baris
petals=int(petal)
rows=int(row)
ts.screensize(int(pixel*row),int(pixel*pixel))  #mengatur screensize 
while pixel>=0 and petal>=0 and row>=0:         #membuat kondisi dimana input harus bilangan positif
    #Flower
    t.up() 
    t.pensize(2)        #mengatur ketebalan pen             
    t.setpos(0,100)     #mengatur koordinat menjadi lebih keatas
    for i in range (petals):        #membuat loop setiap 1 petal dibuat
        head = t.heading()          #memposisikan posisi awal kepala pen
        t.color(random.randint(0,255),random.randint(0,255),random.randint(0,255))  #membuat random warna
        t.down()                    #memulai gambar
        t.circle(150,60)            #membentuk lingkaran 60 derajat dengan radius 150
        t.left(120)                 #memutar arah kepala pen 120 derajat
        t.circle(150,60)            #membentuk lingkaran 60 derajat dengan radius 150
        t.setheading(head)          #mengembalikan posisi semula kepala pen 
        t.left(360/petal)           #mengatur posisi kepala pen dalam membuat petal baru
        t.up()                      #selesai membuat petal
    t.home              #memposisikan pen ke posisi awal


    #Chessboard
    t.setpos(-1/2*row*pixel,-(100+pixel))  #mengatur posisi agar menjadi center dan dibawah bunga
    t.pensize(1)    #mengatur ketebalan pen
    counter=0       #counter kolom n
    for i in range (rows):      #membuat loop untuk membentuk kolom
        for i in range(rows):   #membuat loop untuk membentuk baris
            t.begin_fill()      #awal pengisian warna kotak
            t.color(random.randint(0,255),random.randint(0,255),random.randint(0,255))  #membuat random warna
            for i in range (4):     #membuat loop untuk membentuk persegi
                t.down()            #memulai gambar
                t.forward(pixel)    #maju sesuai panjang yang kita ingin
                t.left(90)          #memutar arah sebesar 90 derajat
                t.up()              #selesai membuat kotak
            t.end_fill()        #akhir pengisisan warna kotak
            t.forward(pixel)    #maju untuk membuat kotak berikutnya
        t.up()                  #selesai membuat baris
        counter+=1              #counter akan bertambah 1 setiap kolom yang telah dibuat
        t.goto(-1/2*row*pixel,-(100+pixel+counter*pixel))     #memindahkan ke kolom berikutnya    
    t.hideturtle()                      #menghilangkan kepala pen
    t.goto(0,-(150+counter*pixel))      #mengatur posisi tulisan
    t.color('blue')                     #warna tulisan
    t.write('Colorful Chessboard of ' + str(rows*rows) + ' Squares and Flower of ' + str(petals) + ' Petals', True, 'center',('Arial','16','bold'))     #menuliskan deskripsi gambar
   
    turtle.exitonclick()                #keluar ketika kita mengklik gambar tsb
    break

else:                                   #memberikan program lain jika input tidak memenuhi syarat
    t.up()
    t.hideturtle()
    t.color('red')
    t.write('INVALID INPUT', True, 'center',('Arial','16','bold'))
    turtle.exitonclick()









