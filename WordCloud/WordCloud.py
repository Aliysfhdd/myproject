import string       
import operator
print('Program to create word cloud from a text file')
print('The result is stored as an HTML file')
print('--------------------------------------------------------','\n')
tanda_baca = string.punctuation                         #variabel untuk tanda baca
Larangan = open('stopWords.txt', 'r')                   #variabel untuk stopWord
larangan= Larangan.read().lower().split()                      #mengecilkan font
while True:                                                 
    try:
        file1 = str(input('Please enter the File(.txt):'))      #input nama file(tanpa jenis file)
        fileinput= open(file1+'.txt', 'r')                   #file input
        break
    except FileNotFoundError:                                 #pengecualian jika file yang diinput tidak ada di direktori
        print("The file",file1,"doesn't exist!!")
        
isi= fileinput.read()  
isi = isi.replace('\n',' ')                             #menghapus enter setiap kalimat
kalimat= isi.lower()                                    #huruf kecil semua
kata = kalimat.split()                                  #menjadikan list kata


#membagi kalimat per kata dan menghapus tanda baca diujung kata
hasil= ''               #penampungan dalam string
for karakter in kata:
        karakter = karakter.strip(tanda_baca)  #menghapus tanda baca diujung kata         
        hasil += str(karakter + ' ')           #menambahkan spasi antar kata
hasil = hasil.replace('  ', ' ')               #menghapus double spasi
hasil = hasil.split()                          #menjadikan list kata


#menghapus stop word di tiap kata
Char= []                #penampungan dalam list
for char in hasil:
    if char not in larangan:        #jika kata tersebut bukan stop word, akan ditampung di Char  
        Char.append(char)

#mencari kata yang sama
kata_sama=[]            #penampungan dalam list
for word in Char:
        if word not in kata_sama:       #jika kata tersebut bukan kata yang sudah ditampung di kata_sama, maka akan ditampung
                kata_sama += [word]

#menghitung jumlah muncul
jumlah = []             #penampungan dalam list
for word in kata_sama:
    jumlah+= [int(Char.count(word))]  #tiap kata_sama akan dihitung jumlah kemunculan di anggota Char

#menggabung kata dan jumlah nya
hasil = []              #penampungan dalam list
for i in range (len(kata_sama)):
        hasil.append ((str(kata_sama[i]),jumlah[i]))    #menggabung kata dan jumlahnya

#sort kata dari yang terbanyak muncul
hasil.sort(key=operator.itemgetter(1,0), reverse= True) #mengurutkan secara terbesar berdasarkan index 1 (jumlah) & selanjutnya diurutkan berdasarkan index 0 (kata)
hasil= hasil[:50]        #memberi batasan 50 kata tertinggi                  


#list yang akan muncul di shell
print(file1 + '.txt')
print('TOP 50 WORDS', '\n')
a=0
for x in range (13):            #membuat 13 baris
        for i in range (4):     #membuat 4 kolom
                if (i+a) >= len(hasil):         #pengecualian jika jumlah kata kurang dari tempat yang disediakan
                        break
                else:
                        print('{:2d} : {:15s}'.format(hasil[(i+a)][1], hasil[(i+a)][0]), end='')        #print list
        a+=4
        print(end='\n')
print('*(count : word)')

#mengurutkan berdasarkan abjad untuk di HTML
hasil.sort(key=operator.itemgetter(0))

#pembuatan HTML
from htmlFunctions import*
jumlah.sort(reverse=True)               #mengsort jumlah kata 
Jumlah= jumlah[:50]                     #batas 50
        
pairs = hasil                           
high_count=int(max(Jumlah))
low_count=int(min(Jumlah))
body=''
for word,cnt in pairs:
        body = body + " " + make_HTML_word(word,cnt,high_count,low_count)
box = make_HTML_box(body)  # creates HTML in a box
print_HTML_file(box,'A Word Cloud of '+(file1)+'.txt')  # writes HTML to file name 'testFile.html'

fileinput.close()

print('\n''--------------------------------------------------------')
print('End of the program')

